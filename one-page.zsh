#!/bin/zsh
# Copyright (C) 2022 Viktor Grigorov
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
# PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License along
# with this program. If not, see https://www.gnu.org/licenses/agpl-3.0.html.
#
#
'emulate' '-LR' 'zsh';
setopt extendedglob pipefail;
whence tectonic perl python >&/dev/null || return 2;
cd ${0:h:A} || return 1;
[[ -s ${1:-b.tex} && -r ${1:-b.tex} && $(file -bi ${1:-b.tex}) =~ '\btext/x-tex\b' ]] || return 3;
typeset q;
typeset -i w=1;
typeset -r \
  unit=pt \
  n=$'\n' \
  file=${1:-b.tex} \
  tmp=$(cat /dev/random | tr -cd 0-9A-Za-z | dd bs=1 count=8 2>&/dev/null || true);
typeset -ri \
  ini=$(cat ${file} | perl -ne '(/\\begin\{document\}/) ? print $. : true') \
  fin=$(cat ${file} | perl -ne '(/\\end\{document\}/)   ? print $. : true') \
  wm=12 ; # weird vertical margin in pt   wm=$((12+$(perl -ne '/\bbmv=\d*?\.?(\d+)/&&print $1' b.tex))) ; # weird vertical margin in pt
((ini && fin)) || return 92;
perl -ne 'if ($. >'${ini}' && $. < '${fin}') { s/^\h*\}\*{0,2}$/}**/gm }; print' ${file} 2>&/dev/null >${tmp};
typeset -ar sides=( ${(f)"$(tectonic -p -r 0 ${tmp} |& perl -ne '/\(\d+,\d+\)/ && print')"} );
((${#sides})) || return 93;
typeset -ra res=( ${(f)"$(python -c "import sys,rpack${n}sides=[${(j , )sides}]${n}coords=rpack.pack(sides)${n}sys.stdout.write(str(rpack.bbox_size(sides,coords))+'\n')${n}for i in coords: sys.stdout.write(str(i)+'\n')")"} );
typeset -ra \
  bbox=( ${(f)"$(printf %s\\n ${res[1]} | perl -pe 's/\((\d+)\h*,\h*(\d+)\)/\1\n\2\n/g')"} ) \
  coord=( ${res[2,-1]} );
perl -pe "BEGIN{undef$/}s/(\h*]\h*{\h*geometry\h*})/paperheight=${bbox[1]}${unit},\npaperwidth=${bbox[2]}${unit},\n\1/" ${tmp} >${tmp}.;
for q ( ${(qqf)"$(cat ${tmp}.)"} ) {
  if [[ ${(Q)q} =~ '^\h*\\block\{[^}]*\}\{%?.*$' ]] {
    # printf %s\\n "\block{${${coord[w]#*, }%?}${unit},${${coord[w]%,*}#?}${unit}}{%"; w=$((w+1));
    printf %s\\n "\block{${${coord[w]#*, }%?}${unit},$((${${coord[w]%,*}#?}-wm))${unit}}{%"; w=$((w+1));
  } elif [[ ${(Q)q} =~ '^\h*\}\*\*\h*.*$' ]] {
    printf %s\\n \};
  # } elif [[ ${(Q)q} == *\\clearpage[A-Za-z@]#|[{,[:blank:]]##landscape[,}%[:blank:]]* ]] {
  } elif [[ ${(Q)q} =~ '\\clearpage\b|\blandscape\b' ]] { # assuming you're not nefariously using either macro or word
  } else {
    printf %s\\n ${(Q)q};
  };
} >${tmp};
tectonic -p -r 0 ${tmp} |& perl -ne '/[wW]arning/i || print';
((?)) || za ${tmp}.pdf& # open with pdfviewer of choice
# ((?)) || qpdfview ${tmp}.pdf& # open with pdfviewer of choice
disown %%; # if pdfviewer doesn't disown
rm -f ${tmp}.;

# to test for aspect ratio
# for q ( *pdf(oc) ) {; <<<${(f)"$(pdfinfo $q)"} while { read -rt1 w } { [[ $w =~ '^Page size:\h*(\d+\.?\d*)\h*\S+\h*(\d+\.?\d*)' ]] && printf %s\\n $((1.0 * ${match[1]} / ${match[2]})) } }
