.POSIX:
cmd:=$(shell if which tectonic >/dev/null; then printf %s 'tectonic -pkr 0'; elif which xelatex >/dev/null; then printf %s 'xelatex'; else printf %s 'false'; fi)
in:=*.tex
out:=$(in:.tex=.pdf)
all:$(out)
$(out):$(in);$(cmd) $<
.PHONY:all